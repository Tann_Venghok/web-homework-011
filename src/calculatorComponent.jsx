import React from "react";
import "./index.css";
import {
  Container,
  Row,
  Col,
  ListGroup,
  Image,
  Form,
  Button,
} from "react-bootstrap";


class FirstBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = { resultHistory: [] };
    this.calculateNumber = this.calculateNumber.bind(this);
  }

  calculateNumber(e) {
    e.preventDefault();
    var a = this.refs.value1.value;
    var b = this.refs.value2.value;
    var newResult = 0;
    var newArray = this.state.resultHistory;

    var option = this.refs.option.value;
    if (option === "1") {
      newResult = Number(a) + Number(b);
    } else if (option === "2") {
      newResult = Number(a) - Number(b);
    } else if (option === "3") {
      newResult = Number(a) * Number(b);
    } else if (option === "4") {
      newResult = Number(a) / Number(b);
    } else if (option === "5") {
      newResult = Number(a) % Number(b);
    }

    // Note: research this part more, why does it work this way?
    newArray.push(newResult);
    this.setState({
      resultHistory: newArray,
    });

  }

  render() {
    return (
      <div className="box1">
        <Row style={{ justifyContent: "center" }}>
          <Col>
            <Container style={{ display: "inline-block" }}>
              <Form.Group>
                <Image src="/Calculator.png" rounded />
                <Form.Control ref="value1" className="inputBox" type="Number" />
                <br />
                <Form.Control ref="value2" className="inputBox" type="Number" />
                <br />
                <Form.Control as="select" ref="option" custom>
                  <option value="1">+ Plus</option>
                  <option value="2">- Substraction</option>
                  <option value="3">x Multiply</option>
                  <option value="4">/ Divide</option>
                  <option value="5">% Mod</option>
                </Form.Control>
                <br />
              </Form.Group>
              <Button variant="outline-primary" onClick={this.calculateNumber}>
                Calculate
              </Button>
            </Container>
          </Col>

          <Col >
            <Container style={{ display: "inline-block" }}>
              <h2>Result History</h2>
              <ListGroup>
                <ul>
                  {this.state.resultHistory.map((item) => (
                    <li key={item}>{item}</li>
                  ))}
                </ul>
              </ListGroup>
            </Container>
          </Col>
        </Row>
      </div>
    );
  }
}

class Calculator extends React.Component {
  render() {
    return (
      <div>
        <FirstBox />
      </div>
    );
  }
}

export default Calculator;
